package homeworkpatterns.builder;

public class BuilderMain {
    public static void main(String[] args) {
        Person person = new Person.Builder()
                .setName("Vova")
                .setSurname("Grakov")
                .setAge(20)
                .build();
        System.out.println(person.getName());
    }
}
