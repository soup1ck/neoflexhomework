package homeworkpatterns.builder;

import lombok.Getter;

@Getter
public class Person {

    private String name;
    private String surname;
    private int age;

    public static class Builder {

        private final Person newPerson;

        public Builder() {
            newPerson = new Person();
        }

        public Builder setName(String name) {
            newPerson.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            newPerson.surname = surname;
            return this;
        }

        public Builder setAge(int age) {
            newPerson.age = age;
            return this;
        }

        public Person build() {
            return newPerson;
        }
    }
}
