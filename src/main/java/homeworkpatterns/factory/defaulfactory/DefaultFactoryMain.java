package homeworkpatterns.factory.defaulfactory;

public class DefaultFactoryMain {
    public static void main(String[] args) {
        CoffeeMachine coffeeMachine = new CoffeeMachine();
        NeoflexCoffeeShop neoflexCoffeeShop = new NeoflexCoffeeShop(coffeeMachine);
        neoflexCoffeeShop.start(CoffeeType.AMERICANO);
        neoflexCoffeeShop.start(CoffeeType.ESPRESSO);
    }
}
