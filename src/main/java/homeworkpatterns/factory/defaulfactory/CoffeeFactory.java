package homeworkpatterns.factory.defaulfactory;

public class CoffeeFactory {

    public ICoffee getCoffee(CoffeeType coffeeType) {
        ICoffee coffee;
        switch (coffeeType) {
            case ESPRESSO -> coffee = new Espresso();
            case AMERICANO -> coffee = new Americano();
            default -> throw new IllegalArgumentException("Данный вид кофе невозможно приготовить в настоящий момент");
        }
        return coffee;
    }
}
