package homeworkpatterns.factory.defaulfactory;

public enum CoffeeType {
    ESPRESSO,
    AMERICANO
}
