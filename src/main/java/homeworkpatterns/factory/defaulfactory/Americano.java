package homeworkpatterns.factory.defaulfactory;

public class Americano implements ICoffee {

    @Override
    public String getCoffeeName() {
        return "Американо";
    }

    @Override
    public void brewCoffee() {
        System.out.println("Завариваю американо");
    }

    @Override
    public void pourCoffee() {
        System.out.println("Наливаю американо в чашку");
    }
}
