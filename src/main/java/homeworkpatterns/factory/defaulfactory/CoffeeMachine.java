package homeworkpatterns.factory.defaulfactory;

public class CoffeeMachine {

    public void makeCoffee(ICoffee coffee) {
        System.out.println("Началось приготовление кофе " + coffee.getCoffeeName());
    }
}
