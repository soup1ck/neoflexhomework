package homeworkpatterns.factory.defaulfactory;

public class Espresso implements ICoffee {

    @Override
    public String getCoffeeName() {
        return "Эспрессо";
    }

    @Override
    public void brewCoffee() {
        System.out.println("Завариваю эспрессо");
    }

    @Override
    public void pourCoffee() {
        System.out.println("Наливаю эспрессо в чашку");
    }
}
