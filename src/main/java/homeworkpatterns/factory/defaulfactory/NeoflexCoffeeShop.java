package homeworkpatterns.factory.defaulfactory;

public class NeoflexCoffeeShop {

    private final CoffeeMachine coffeeMachine;

    public NeoflexCoffeeShop(CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
    }

    public void start(CoffeeType coffeeType) {
        CoffeeFactory coffeeFactory = new CoffeeFactory();
        ICoffee coffee = coffeeFactory.getCoffee(coffeeType);
        coffeeMachine.makeCoffee(coffee);
        coffee.brewCoffee();
        coffee.pourCoffee();
    }
}
