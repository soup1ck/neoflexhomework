package homeworkpatterns.factory.defaulfactory;

public interface ICoffee {

    String getCoffeeName();

    void brewCoffee();

    void pourCoffee();
}
