package homeworkpatterns.factory.abstractfactory;

public class BlackTea implements ITea {

    @Override
    public String getTeaName() {
        return "Черный чай";
    }

    @Override
    public void brewTea() {
        System.out.println("Завариваю черный чай");
    }

    @Override
    public void pourTea() {
        System.out.println("Наливаю черный чай в кружку");
    }
}
