package homeworkpatterns.factory.abstractfactory;

public class GreenTea implements ITea {

    @Override
    public String getTeaName() {
        return "Зеленый чай";
    }

    @Override
    public void brewTea() {
        System.out.println("Завариваю зеленый чай");
    }

    @Override
    public void pourTea() {
        System.out.println("Наливаю зеленый чай в кружку");
    }
}
