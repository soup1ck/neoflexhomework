package homeworkpatterns.factory.abstractfactory;


import homeworkpatterns.factory.defaulfactory.Espresso;
import homeworkpatterns.factory.defaulfactory.ICoffee;

public class BlackTeaAndEspresso implements DrinkFactory {

    @Override
    public ICoffee createCoffee() {
        return new Espresso();
    }

    @Override
    public ITea createTea() {
        return new BlackTea();
    }
}
