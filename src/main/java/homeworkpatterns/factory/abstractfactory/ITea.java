package homeworkpatterns.factory.abstractfactory;

public interface ITea {

    String getTeaName();

    void brewTea();

    void pourTea();
}
