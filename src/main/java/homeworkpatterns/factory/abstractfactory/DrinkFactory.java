package homeworkpatterns.factory.abstractfactory;

import homeworkpatterns.factory.defaulfactory.ICoffee;

public interface DrinkFactory {

    ICoffee createCoffee();

    ITea createTea();
}
