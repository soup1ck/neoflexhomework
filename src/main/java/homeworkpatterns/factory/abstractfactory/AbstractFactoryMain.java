package homeworkpatterns.factory.abstractfactory;

public class AbstractFactoryMain {
    public static void main(String[] args) {
        DrinkFactory drinkFactory;
        drinkFactory = new BlackTeaAndEspresso();
        System.out.println(drinkFactory.createCoffee().getCoffeeName());
        System.out.println(drinkFactory.createTea().getTeaName());
        drinkFactory = new GreenTeaAndAmericano();
        System.out.println(drinkFactory.createCoffee().getCoffeeName());
        System.out.println(drinkFactory.createTea().getTeaName());
    }
}
