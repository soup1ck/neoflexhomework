package homeworkpatterns.factory.abstractfactory;

import homeworkpatterns.factory.defaulfactory.Americano;
import homeworkpatterns.factory.defaulfactory.ICoffee;

public class GreenTeaAndAmericano implements DrinkFactory {

    @Override
    public ICoffee createCoffee() {
        return new Americano();
    }

    @Override
    public ITea createTea() {
        return new GreenTea();
    }
}
