package homeworkpatterns.singleton;


import homeworkpatterns.factory.defaulfactory.Espresso;

import java.util.ArrayList;
import java.util.List;

public class SingletonMain {
    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        TestClass testClass1 = new TestClass();
        Espresso espresso = new Espresso();
        List<Object> objects = new ArrayList<>();
        List<Object> objects1 = new ArrayList<>();
        objects.add(testClass);
        objects.add(testClass1);
        objects1.add(testClass);
        objects1.add(espresso);
        LoggerSingleton logger = LoggerSingleton.getInstance();
        logger.logInfo(objects);
        LoggerSingleton logger1 = LoggerSingleton.getInstance();
        logger1.logInfo(objects1);
        System.out.println(logger.hashCode());
        System.out.println(logger1.hashCode());
    }
}
