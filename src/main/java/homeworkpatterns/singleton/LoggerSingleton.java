package homeworkpatterns.singleton;

import java.util.List;

public class LoggerSingleton {

    private static LoggerSingleton loggerInstance;

    private LoggerSingleton() {
    }

    public static LoggerSingleton getInstance() {
        if (loggerInstance == null) {
            loggerInstance = new LoggerSingleton();
        }
        return loggerInstance;
    }

    public void logInfo(List<Object> objects) {
        System.out.print("log info: ");
        objects.forEach(o -> System.out.print(o.getClass().getName() + " "));
        System.out.println();
    }

    public void logInfo(Object object) {
        System.out.print("log info: " + object.getClass().getName());
    }
}
