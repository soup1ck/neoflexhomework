package homeworkpatterns.proxy;

public interface IConnection {

    void connect(String name);
}
