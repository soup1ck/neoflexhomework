package homeworkpatterns.proxy;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class DatabaseConnectionProxy implements IConnection {

    private DatabaseConnection databaseConnection;
    private final Map<String, String> cache = new HashMap<>();


    @Override
    public void connect(String name) {
        if (!cache.containsKey(name)) {
            String databaseName = databaseConnection.getDatabaseName();
            cache.put(name, databaseName);
            System.out.println("localhost:port/" + name);
        } else {
            System.out.println("localhost:port/" + cache.get(name));
        }
    }
}
