package homeworkpatterns.proxy;

public class ProxyMain {
    public static void main(String[] args) {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        databaseConnection.setDatabaseName("users");
        DatabaseConnection databaseConnection1 = new DatabaseConnection();
        databaseConnection1.setDatabaseName("users");
        DatabaseConnectionProxy databaseConnectionProxy = new DatabaseConnectionProxy(databaseConnection);
        databaseConnectionProxy.connect(databaseConnection.getDatabaseName());
        databaseConnectionProxy.connect(databaseConnection1.getDatabaseName());
    }
}
