package homeworkpatterns.proxy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatabaseConnection implements IConnection {

    private String databaseName;

    @Override
    public void connect(String name) {
        System.out.println("Подключение к базе данных " + name + " установлено");
    }
}
