package homeworkpatterns.adapter;

public interface UsbSlot {

    void insert();
}
