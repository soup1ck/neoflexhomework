package homeworkpatterns.adapter;

public class AdapterMain {
    public static void main(String[] args) {
        Computer computer = new Computer();
        MemoryCard memoryCard = new MemoryCard();
        MemoryCardAdapter memoryCardAdapter = new MemoryCardAdapter(memoryCard);
        computer.connect(memoryCardAdapter);
        Computer computer1 = new Computer();
        FlashDrive flashDrive = new FlashDrive();
        computer1.connect(flashDrive);
    }
}
