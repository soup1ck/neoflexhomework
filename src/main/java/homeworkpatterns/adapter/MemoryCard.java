package homeworkpatterns.adapter;

public class MemoryCard implements MemoryCardSlot {

    @Override
    public void insert() {
        System.out.println("Карта памяти вставлена");
    }
}
