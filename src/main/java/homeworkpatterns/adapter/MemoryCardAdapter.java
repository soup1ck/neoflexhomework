package homeworkpatterns.adapter;

public class MemoryCardAdapter implements UsbSlot {

    private final MemoryCard memoryCard;

    public MemoryCardAdapter(MemoryCard memoryCard) {
        this.memoryCard = memoryCard;
    }

    @Override
    public void insert() {
        memoryCard.insert();
    }
}
