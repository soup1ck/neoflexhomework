package homeworkpatterns.adapter;

public class Computer {

    public void connect(UsbSlot usbSlot) {
        usbSlot.insert();
        System.out.println("USB слот подключен");
    }
}
