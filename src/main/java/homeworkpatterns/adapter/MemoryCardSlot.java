package homeworkpatterns.adapter;

public interface MemoryCardSlot {

    void insert();
}
