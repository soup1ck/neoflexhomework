package homeworkpatterns.adapter;

public class FlashDrive implements UsbSlot {

    @Override
    public void insert() {
        System.out.println("Флешка вставлена");

    }
}
